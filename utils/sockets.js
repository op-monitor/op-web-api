let dgram = require('dgram');
let objectUtils = require('./objects');
let wait = ms => new Promise((r, j)=>setTimeout(r, ms));

/**
 * Send a json packet to a given address.
 * @param address: {host, port}
 * @param data: {...}
 * @returns {Promise<{status: boolean, message: string}>}
 */
const sendUdp = async function (address, data) {
    const {host, port} = address;
    let resp = {
        status: false,
        message: ""
    };

    if (!data)
        console.log(`Didn't received data:${data}`);

    if (data && objectUtils.isObject(data))
        data = JSON.stringify(data); // convert the json to bytes.

    let client = dgram.createSocket('udp4');

    client.send(data, 0, data.length, port, host, function (err, bytes) {
        client.on('listening', function() {
            let address = client.address();
            console.log('UDP Server listening on ' + address.address + ':' + address.port);

        });
        client.on('message', function (message, remote) {
            console.log(remote.address + ':' + remote.port + ' - ' + message);
            const messageJson = JSON.parse(message);
            resp.message = messageJson.message;
            if (messageJson.status === 'ok')
                resp.status = true;
            client.close();
        });
        client.on('error', (err) => {
            console.log(`socket error:\n${err.stack}`);
            client.close();
        });
    });
    await wait(2000);
    return resp
};


const ping = async function (address){
   return sendUdp(address, {action: 'ping'});
};
module.exports = {sendUdp, ping};