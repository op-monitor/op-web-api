let isObject = function (obj) {
    return obj != null && obj.constructor.name === "Object"
};

module.exports = {isObject};