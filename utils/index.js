const dates = require('./dates');
const objects = require('./objects');
const sockets = require('./sockets');

module.exports = {
    dates,
    objects,
    sockets
};