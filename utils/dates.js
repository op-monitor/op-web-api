const dateformat = require('dateformat');

const DATETIME_FORMAT = "";

/**
 * Return a datetime of the current date
 */
const getDate = () => {
    return dateformat(new Date(), "dd-mm-yyyy HH:MM:ss");
};


module.exports = {getDate};