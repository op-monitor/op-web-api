let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');

let {sockets} = require('../utils');
let {AgentStorage} = require('../op-data-types/');


/**
 * Return list of all the registered `Agents`
 * @query {string} populate: return the `Monitors` values as objects or not. [optional]
 * @example: /api/agent?populate [optional]
 */
router.get('/', function (req, res, next) {
    const {populate} = req.query;
    let populateFields = populate !== undefined ? ['monitors'] : [];

    AgentStorage.find().populate(populateFields)
        .then(data => {
            res.json({status: "success", data});
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure", message: err});
        })
});

/**
 * Return Agent's data by given id
 * Including Agent metadata and its registered Monitors
 * @param {string} id: the Agent's id
 * @query {string} populate: return the `Monitors` values as objects or not. [optional]
 */
router.get('/:id', function (req, res, next) {
    const {id} = req.params;
    const {populate} = req.query;
    let populateFields = populate !== undefined ? ['monitors'] : [];

    AgentStorage.findById(id).populate(populateFields)
        .then(data => {
            res.json({status: "success", data});
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure", message: err});
        })
});

/**
 * Update an existing Agent
 * @param {string} id: the Agent's id
 * @body {object} data: the Agent's data as an object
 */
router.post('/:id', function (req, res, next) {
    const {id} = req.params;
    const {data} = req.body;
    AgentStorage.findOneAndUpdate({_id: id}, data, {new: true})
        .then(data => {
            if (data) {
                res.json({status: "success", data});
            }
            else {
                res.status(400).json({status: 'failure', message: `couldn't resolve monitor id ${id}`});
            }

        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure", message: err});
        })
});

/**
 * Add a new Agent.
 * The Agent service need to be active
 * This endpoint is for adding monitoring (without the installation part)
 * @body {object} data: the Agent's data as an object: {port: 0, host: ''}
 */
router.post('/', async function (req, res, next) {
    const {data: {host, port}} = req.body;
    if (!host || !port) {
        res.status(400).json({status: "failure", message: `some required info is missing`});
        return
    }
    if (typeof host !== "string" || isNaN(port)) {
        res.status(400).json({status: "failure", message: `given information isn't as the required format`});
        return
    }

    const ping = await sockets.ping({host, port});

    if (!ping.status) {
        res.status(502).json({
            status: "failure",
            message: `fail to ping server - ${host}:${port}, err: ${ping.message}\``
        });
        return
    }

    AgentStorage.create({host, port, _id: new mongoose.Types.ObjectId()})
        .then(data => {
            res.json({status: "success", data});
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure", message: err});
        })
});

/**
 * Delete existing Agent.
 * The Agent service need to be active
 * This endpoint is for deleting it's monitoring (without the un-installation part)
 * Deleting an Agent with registered Monitors is not supported.
 * @param {string} id: the Agent's id
 */
router.delete('/:id', async function (req, res, next) {
    const {id} = req.params;
    if (!id)
        res.status(400).json({status: 'failure', message: `didn't received id parameter`});

    agent = await AgentStorage.findById(id);
    if (agent.monitors.length > 0) {
        res.status(400).json({status: 'failure', message: `This Agent had registered monitors`});
        return;
    }

    AgentStorage.deleteOne({_id: id})
        .then(data => {
            if (data) {
                res.json({status: "success", data});
            }
            else {
                res.status(400).json({status: 'failure', message: `couldn't find monitor id ${id}`})
            }
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure", message: err});
        });
});


module.exports = router;
