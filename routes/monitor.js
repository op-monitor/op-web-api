let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');

let {sockets} = require('../utils');
let {MonitorStorage, AgentStorage} = require('../op-data-types/');


/**
 * Return list of all the registered `Monitors`
 * @query {string} populate: populate the `agents` field.
 * @example: /api/monitor?populate [optional]
 */
router.get('/', function (req, res, next) {
    const {populate} = req.query;
    let populateFields = populate !== undefined ? ['agents'] : [];

    MonitorStorage.find().populate(populateFields)
        .then(data => {
            res.json({status: "success", data});
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure"});
        })
});

/**
 * Return Monitor's data by given id
 * Including Monitor metadata and its registered-agents
 * @param {string} id: the monitor's id.
 * @query {string} populate: populate the `agents` field. [optional]
 * @example /api/monitor/5d67efe1d9412ad3825853f9?populate
 */
router.get('/:id', function (req, res, next) {
    const {id} = req.params;
    const {populate} = req.query;
    let populateFields = populate !== undefined ? ['agents'] : [];

    MonitorStorage.findById(id).populate(populateFields)
        .then(data => {
            res.json({status: "success", data});
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure"});
        })

});

/**
 * Update Monitor metadata
 * @param {string} id: the Monitor's id
 * @body {object} data: the Monitor's data as an object
 */
router.post('/:id', function (req, res, next) {
    const {id} = req.params;
    const {data} = req.body;
    MonitorStorage.findOneAndUpdate({_id: id}, data, {new: true})
        .then(data => {
            if (data) {
                res.json({status: "success", data});
            }
            else {
                res.status(400).json({status: 'failure', message: `couldn't resolve monitor id ${id}`})
            }
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure"});
        })
});

/**
 * Add new monitor in the system.
 * The monitor service need to be active
 * This endpoint is for adding monitoring (without the installation part)
 * @body {object} data: the Monitor's data as an object: {port: 0, host: ''}
 */
router.post('/', async function (req, res, next) {
    const {data: {host, port}} = req.body;
    if (!host || !port) {
        res.status(400).json({status: "failure", message: `some required info is missing`});
        return;
    }
    if (typeof host !== "string" || isNaN(port)) {
        res.json({status: "failure", message: `given information isn't as the required format`});
        return;
    }

    const ping = await sockets.ping({host, port});

    if (!ping.status) {
        res.status(502).json({
            status: "failure",
            message: `fail to ping server - ${host}:${port}, err: ${ping.message}\``
        });
        return;
    }

    MonitorStorage.create({host, port, _id: new mongoose.Types.ObjectId()})
        .then(data => {
            res.json({status: "success", data});
        })
        .catch(err => {
            console.error(err);
            res.state(400).json({status: "failure"});
        })

});

/**
 * Delete an existing Monitor.
 * The monitor service need to be active
 * This endpoint is for deleting it's monitoring (without the un-installation part)
 * Deleting a Monitor with registered Agents is not supported.
 * @param {string} id: the Monitor's id
 * @example: /api/monitor/5d67efe1d9412ad3825853f9
 */
router.delete('/:id', async function (req, res, next) {
    const {id} = req.params;
    if (!id) {
        res.status(400).json({status: 'failure', message: `didn't received id parameter`});
        return;
    }

    monitor = await MonitorStorage.findById(id);
    if (monitor.agents.length > 0) {
        res.status(400).json({status: 'failure', message: `This monitor had registered agents`});
        return;
    }

    MonitorStorage.deleteOne({_id: id})
        .then(data => {
            if (data) {
                res.json({status: "success", data});
            }
            else {
                res.status(400).json({status: 'failure', message: `couldn't find monitor id ${id}`});
            }

        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure"});
        });
});

/**
 * Registered/Unregister new agent to Monitor
 * @query {string} action - a string represent the requested action, one of: [register, unregister]
 * @param {string} mid - the monitor id
 * @param {string} aid - the agent id
 * @example: /api/monitor/5d67efe1d9412ad3825853f9/agent/5d67efbad9412ad3825853d4?action=register
 */
router.post('/:mid/agent/:aid', async function (req, res, next) {
    const {action} = req.query;
    const {mid, aid} = req.params;
    if (!['unregister', 'register'].find(prop => action === prop)) {
        res.status(400).json({status: 'failure', message: `received unsupported action: ${action}`});
        return;
    }
    if (!mid || !aid) {
        res.status(400).json({
            status: 'failure',
            message: `didn't received AgentId or MonitorId. aid: ${aid}, mid: ${mid}`
        });
        return;
    }

    // Send request to the Monitor mid.
    let monitor = await MonitorStorage.findOne({_id: mid});
    let agent = await AgentStorage.findOne({_id: aid});

    let resp = await sockets.sendUdp({host: monitor.host, port: monitor.port}, {
        action,
        agent: {host: agent.host, port: agent.port}
    });
    if (!resp.status) {
        res.status(502).json({
            status: 'failure',
            message: resp.message ? resp.message : 'fail to receive ack from Monitor'
        });
        return;
    }

    // If retrieved ack - validate it and make changes in DB.
    if (action === 'register') {
        monitor.agents.push(agent);
        agent.monitors.push(monitor);
    }
    else { // action==='unregister'
        monitor.agents = monitor.agents.filter(a => a.toString() !== aid);
        agent.monitors = agent.monitors.filter(m => m.toString() !== mid);
    }
    monitor.save();
    agent.save();

    res.json({status: 'success'});
});


module.exports = router;
