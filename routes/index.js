let express = require('express');
let router = express.Router();
let monitor = require('./monitor');
let agent = require('./agent');
let report = require('./report');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/monitor', monitor);
router.use('/agent', agent);
router.use('/report', report);

module.exports = router;
