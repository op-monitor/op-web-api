const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const {ReportStorage, MonitorStorage, AgentStorage} = require('../op-data-types/');


/**
 * Return list Reports
 * supports `monitor` and `agent` fields on query
 * @query {string} mid - the monitor id [optional]
 * @query {string} aid - the agent id [optional]
 * @query: populate: populate the `monitor, agent` fields. [optional]
 */
router.get('/', function (req, res, next) {
    const {monitor: mid, agent: aid, populate} = req.query;

    let populateFields = populate !== undefined ? ['monitor', 'agent'] : [];

    let findQuery = {};

    if (mid)
        findQuery.monitor = mid;
    if (aid)
        findQuery.agent = aid;

    ReportStorage.find({...findQuery}).populate(populateFields)
        .then(data => {
            res.json({status: "success", data});
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure", message: err});
        })
});

/**
 * Get a Report by given id
 * @param {string} id - the Report id
 * @query: populate: populate the `monitor, agent` fields. [optional]
 */
router.get('/:id', function (req, res, next) {
    const {id} = req.params;
    const {populate} = req.query;

    let populateFields = populate !== undefined ? ['monitor', 'agent'] : [];

    ReportStorage.findById(id).populate(populateFields)
        .then(data => {
            res.json({status: "success", data});
        })
        .catch(err => {
            console.error(err);
            res.status(500).json({status: "failure", message: err});
        })
});

/**
 * Add a new Report
 * @body {object} data: the Report's data as an object
 */
router.post('/', async function (req, res, next) {
    try {
        const {
            data,
            task,
            monitor: {
                host: mhost,
                port: mport
            },
            agent: {
                host: ahost,
                port: aport
            },
        } = req.body;

        const monitor = await MonitorStorage.findOne({host: mhost, port: mport});
        const agent = await AgentStorage.findOne({host: ahost, port: aport});
        if (!monitor || !agent) {
            res.status(400)
                .json({
                    status: 'failure',
                    message: `Couldn't find monitor: ${mhost}:${mport} or agent: ${ahost}:${aport}`
                });
            return
        }
        let report = new ReportStorage({
            _id: new mongoose.Types.ObjectId(),
            monitor: monitor._id,
            agent: agent._id,
            data,
            task
        });

        await report.save();
        res.json({status: 'success', data: report});

    } catch (error) {
        if (error.name === 'CastError') {
            res.status(500).json({status: 'failure', message: `Received invalid id; Stacktrace: ${error}`});
        }
        else {
            throw error;
        }
    }
});


module.exports = router;
