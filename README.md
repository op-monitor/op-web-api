# `op-web-api`
An HTTP server handle the access to DB, and resolve actions from the different components.
Build according to REST API principles.

## Endpoints:
For further documentation and examples - see the `/routes` folder.

### Monitor
* Request initialise `api/monitor/`
* Create new \ update \ get Monitors entities
* Resigster \ unregister Agents

### Agent
* Request initialise `api/agent/`
* Create new \ update \ get Agents entities

### Report
* Request initialise `api/report/`
* Create new \ get Report entities
    

## Stack:
* Node.js\Express
* Libs: [mongoose](https://www.npmjs.com/package/mongoose)
