const MonitorModel = require('../op-data-types/models/monitor');
const AgentModel = require('../op-data-types/models/agent');
const ReportModel = require('../op-data-types/models/report');


module.exports = {
    MonitorStorage: MonitorModel,
    AgentStorage: AgentModel,
    ReportStorage: ReportModel
};