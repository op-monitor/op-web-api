const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let monitorSchema = new Schema({
    _id: mongoose.ObjectId,
    host: String,
    port: Number,
    creationDate: { type: Date, default: Date.now },
    agents: {
        type: [{ type: Schema.Types.ObjectId, ref: 'Agent' }],
        default: []
    }
});

module.exports = mongoose.model('Monitor', monitorSchema, 'monitor');

