const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let agentSchema = new Schema({
    _id: mongoose.ObjectId,
    host: String,
    port: Number,
    creationDate: {type: Date, default: Date.now},
    monitors: {
        type: [{ type: Schema.Types.ObjectId, ref: 'Monitor' }],
        default: []
    }
});

module.exports = mongoose.model('Agent', agentSchema, 'agent');

