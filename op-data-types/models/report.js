let mongoose = require('mongoose');
const Schema = mongoose.Schema;

let agentSchema = new Schema({
    _id: mongoose.ObjectId,
    task: String,
    agent: { type: Schema.Types.ObjectId, ref: 'Agent' },
    monitor: { type: Schema.Types.ObjectId, ref: 'Monitor' },
    creationDate:  {type: Date, default: Date.now},
    data: Object
});

module.exports = mongoose.model('Report', agentSchema, 'report');
